# Compilation instructions

1. Set the version number in  package.json

2. Pack the application
Go to 
```sh
resources/app/npm run pack-win
```
```sh
npm run pack-win
```
3. Create the installer executable file for Windows (this process may take some minutes)
```sh
node windows-installer.js "win32" "ia32"
```
4. The .exe file will be created in resources/windows-installer/ as MillsonicInstaller.exe
5. To prevent the version to be overwritten by a new one, rename the file (i.e. MillsonicInstaller_v1.0.22.exe)

6. Upload to https://millsonic.com/sc/apps/$platform/$version/
i.e.:
```sh
https://millsonic.com/sc/apps/win32-ia32/1.0.22/
```
7. Change all other RELEASES files adding the absolute path to each release
i.e.:
```sh
https://millsonic.com/sc/apps/win32-ia32/1.0.22/Millsonic-1.0.22-delta.nupkg
https://millsonic.com/sc/apps/win32-ia32/1.0.22/Millsonic-1.0.22-full.nupkg
```