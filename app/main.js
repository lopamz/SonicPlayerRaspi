// Modules to control application life and create native browser window
const { app, BrowserWindow } = require("electron");
const path = require("path");

//####WINDOWS###
//const updater = require("electron-simple-updater");
//
//updater.init("https://millsonic.com/sc/apps/updates.json");
//
//if (require("electron-squirrel-startup")) app.quit();
//
//let myWindow = null;
//const gotTheLock = app.requestSingleInstanceLock();
//if (!gotTheLock) {
//  app.quit();
//} else {
//  app.on("second-instance", (event, commandLine, workingDirectory) => {
//    // Someone tried to run a second instance, we should focus our window.
//    if (myWindow) {
//      if (myWindow.isMinimized()) myWindow.restore();
//      myWindow.focus();
//    }
//  });
//  // Create myWindow, load the rest of the app, etc...
//  app.on("ready", () => {});
//}
//
//require("update-electron-app")({
//  host: "https://millsonic.com",
//  repo: "sc/apps",
//  updateInterval: "5 minutes",
//  logger: require("electron-log"),
//});

///####WINDOWS

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

// this should be placed at top of main.js to handle setup events quickly
//if (handleSquirrelEvent(app)) {
//  // squirrel event handled and app will exit in 1000ms, so don't do anything else
//  return;
//}

function createWindow() {
  // Create the browser window.
  //  mainWindow = new BrowserWindow({
  //    width: 800,
  //    height: 600,
  //    webPreferences: {
  //                    nodeIntegration: true,
  //      preload: path.join(__dirname, 'build/preload.js')
  //    }
  //  })

  mainWindow = new BrowserWindow({
    width: 550, //550,
    height: 180, //180,
    webPreferences: { nodeIntegration: true },
    /*transparent: true,
    frame: false,*/
    icon: path.join(__dirname, "src/icon/icon.png"),
  });

  mainWindow.setMenuBarVisibility(false);

  let customUserAgent = mainWindow.webContents.getUserAgent() + " MAC: " + JSON.stringify(getMac());
  mainWindow.webContents.setUserAgent(customUserAgent);

  // and load the index.html of the app.
  mainWindow.loadFile("build/index.html");

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on("closed", function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

//###WINDOWS
//function handleSquirrelEvent(application) {
//  if (process.argv.length === 1) {
//    return false;
//  }
//
//  const ChildProcess = require("child_process");
//  const path = require("path");
//
//  const appFolder = path.resolve(process.execPath, "..");
//  const rootAtomFolder = path.resolve(appFolder, "..");
//  const updateDotExe = path.resolve(path.join(rootAtomFolder, "Update.exe"));
//  const exeName = path.basename(process.execPath);
//
//  const spawn = function (command, args) {
//    let spawnedProcess, error;
//
//    try {
//      spawnedProcess = ChildProcess.spawn(command, args, {
//        detached: true,
//      });
//    } catch (error) {}
//
//    return spawnedProcess;
//  };
//
//  const spawnUpdate = function (args) {
//    return spawn(updateDotExe, args);
//  };
//
//  const squirrelEvent = process.argv[1];
//  switch (squirrelEvent) {
//    case "--squirrel-install":
//    case "--squirrel-updated":
//      // Optionally do things such as:
//      // - Add your .exe to the PATH
//      // - Write to the registry for things like file associations and
//      //   explorer context menus
//
//      // Install desktop and start menu shortcuts
//      spawnUpdate(["--createShortcut", exeName]);
//
//      setTimeout(application.quit, 1000);
//      return true;
//
//    case "--squirrel-uninstall":
//      // Undo anything you did in the --squirrel-install and
//      // --squirrel-updated handlers
//
//      const Store = require("electron-store");
//      const store = new Store();
//      store.delete();
//
//      // Remove desktop and start menu shortcuts
//      spawnUpdate(["--removeShortcut", exeName]);
//
//      setTimeout(application.quit, 1000);
//      return true;
//
//    case "--squirrel-obsolete":
//      // This is called on the outgoing version of your app before
//      // we update to the new version - it's the opposite of
//      // --squirrel-updated
//
//      application.quit();
//      return true;
//  }
//}
//
//const appFolder = path.dirname(process.execPath);
//const updateExe = path.resolve(appFolder, "Millsonic.exe"); //appFolder, '..', 'Millsonic.exe'
//const exeName = path.basename(process.execPath);
///*
//app.setLoginItemSettings({
//  openAtLogin: true,
//  path: updateExe,
//  args: [
//    '--processStart', '"${exeName}"',
//    '--process-start-args', '"--hidden"'
//  ]
//})*/
//
//var AutoLaunch = require("auto-launch");
//
//var millsonicAutoLauncher = new AutoLaunch({
//  name: exeName,
//  path: updateExe,
//});
//
//millsonicAutoLauncher.enable();
//
////millsonicAutoLauncher.disable();
//
//millsonicAutoLauncher
//  .isEnabled()
//  .then(function (isEnabled) {
//    if (isEnabled) {
//      return;
//    }
//    millsonicAutoLauncher.enable();
//  })
//
//  .catch(function (err) {
//    // handle error
//  });

function getMac() {
  let allMACs = {};
  const netInters = require("os").networkInterfaces();
  console.log("os networkINterfaces", netInters);
  for (const netInterName in netInters) {
    if (netInters.hasOwnProperty(netInterName)) {
      const netInter = netInters[netInterName];
      const MACs = [];
      let mac = "";
      netInter.forEach((singleNetInter) => {
        if (singleNetInter.hasOwnProperty("mac")) {
          console.log("-->", netInterName, singleNetInter.mac);
          if (
            MACs.indexOf(singleNetInter.mac) == -1 &&
            singleNetInter.mac !== "00:00:00:00:00:00"
          ) {
            MACs.push(singleNetInter.mac);
          }
        }
      });
      if (MACs.length > 0) {
        allMACs[netInterName] = MACs.sort().join("|");
      }
    }
  }
  console.log("allMACs", allMACs);
  return allMACs;
}