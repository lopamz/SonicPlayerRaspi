const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller;
const path = require('path');

let platform = 'win32';
let arch = 'ia32';
for (let j = 0; j < process.argv.length; j++) {
  console.log(j + ' -> ' + (process.argv[j]));
  if(j === 2) {
    platform = process.argv[j];
  }
  if(j === 3) {
    arch = process.argv[j];
  }
}

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error);
    process.exit(1);
  });

function getInstallerConfig () {
  console.log('creating windows installer');
  const rootPath = path.join('./');
  const outPath = path.join('../');
  console.log('rootPath', rootPath);
  console.log('outPath', outPath);

  return Promise.resolve({
    appDirectory: path.join(outPath, 'Millsonic-' + platform + '-' + arch + '/'),
    authors: 'Millsonic',
    noMsi: true,
    outputDirectory: path.join(outPath, 'windows-installer'),
    exe: 'Millsonic.exe',
    setupExe: 'MillsonicInstaller.exe',
    setupIcon: 'windows_icon.ico'
  });
}