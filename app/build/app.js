const remote = require("electron").remote;

/*const app = require.resolve('electron');
console.log(app);*/
let lastTimeInput = 0;
let playerPlaying = false;

const path = require("path");
console.log(path);
console.log(path.dirname(process.execPath));

const versionText = remote.app.getVersion();
const version = versionText.split(".").pop();
console.log("Millsonic " + versionText + " updated version");
$(".version").text("v" + versionText);

$("#player").hide();

$(".min-btn").on("click", function (e) {
  var window = remote.getCurrentWindow();
  window.minimize();
});

$(".close-btn").on("click", function (e) {
  console.log("cierro");
  var window = remote.getCurrentWindow();
  window.close();
});

$(".turn-off-btn").on("click", function (e) {
  let prompt = $(".prompt");
  prompt.css("display", "flex");
});

$("#close-prompt").on("click", function (e) {
  $(".prompt").css("display", "none");
  $(".prompt input.form-control").val("");
});

$(".prompt input.form-control").on("input", function (e) {
  let input = $(e.currentTarget);
  if (input.val() === "yoda") {
    $(".prompt").css("display", "none");
    $(".prompt input.form-control").val("");
    disconect("manual disconnect");
  }
});

$(".input-code-group input").on("input", function (e) {
  var input = $(e.currentTarget);
  var val = input.val();
  if (isNaN(val)) {
    e.preventDefault();
    input.val("");
    return false;
  } else {
    var next = input.next();
    if (next.length > 0) {
      next.focus();
    }
  }
});

$(".input-code-group input").on("paste", function (e) {
  console.log("paste");
  var input = $(e.currentTarget);
  catchPaste(e, input[0], function (val) {
    if (typeof val === "string" && val.length > 0) {
      var arr = Array.from(val);
      $(".input-code-group input").each(function (i, inp) {
        if (typeof arr[i] !== "undefined") {
          $(inp).val(arr[i]);
        }
      });
      $(".input-code-group button").click();
    }
    console.log("paste callback");
  });
});

$(".input-code-group input").focus(function () {
  $(this).select();
});

$(".input-code-group *").on("keyup", function (e) {
  if (e.keyCode == 8) {
    var input = $(e.currentTarget);
    if (input.is("input")) {
      if (input.val() === "") {
        input.prev().val("");
        var prevPrev = input.prev().prev();
        if (prevPrev.length > 0) {
          var prevVal = input.prev().prev().val();
          input.prev().prev().val(prevVal);
          input.prev().prev().focus();
        }
      } else {
        input.val("");
        var prevVal = input.prev().val();
        input.prev().val(prevVal);
        input.prev().focus();
      }
      console.log(input.attr("name"));
    } else {
      input.prev().val("");
      var prevVal = input.prev().prev().val();
      input.prev().prev().val(prevVal);
      input.prev().prev().focus();
    }
    if (input.is(":focus")) {
      console.log("is in focus");
    }
  }
});

function playerPlay() {
  const playerDom = $("#radioplayer").get(0);
  if (playerDom != undefined) {
    playerPlaying = true;
    playerDom.play();
    $(".play-btn").addClass("playing");
  }
}

function playerPause() {
  const playerDom = $("#radioplayer").get(0);
  if (playerDom != undefined) {
    playerPlaying = false;
    playerDom.pause();
    $(".play-btn").removeClass("playing");
  }
}

function playerChangeVolume(volume) {
  const playerDom = $("#radioplayer").get(0);
  if (playerDom != undefined) {
    if (isNaN(volume)) {
      console.log("volumen is NaN");
      return;
    }
    playerDom.volume = volume;
  }
}

$(".play-btn").on("click", function (e) {
  if (playerPlaying) {
    playerPause();
  } else {
    playerPlay();
  }
});

var shell = require("electron").shell;
//open links externally by default
$(document).on("click", 'a[href^="http"]', function (event) {
  event.preventDefault();
  shell.openExternal(this.href);
});

const Store = require("electron-store");
const store = new Store();

const CustomStore = require("./customStore.js");
const cStore = new CustomStore({
  defaults: {
    log: ["no token default " + new Date().toString()],
    radio: null,
  },
  configName: "radio",
});
//VALIDO QUE TENGA GUARDADO LA INFO PARA CUANDO SE CIERRA Y ABRE SOFTWARE

let MACaddresses = getMac();
let radio = store.get("radio");
let cRadio = cStore.get("radio");
console.log(radio);
console.log(
  radio === null ||
    (typeof radio === "object" && radio.hasOwnProperty("btoken"))
);
if (
  radio === null ||
  typeof radio !== "object" ||
  !radio.hasOwnProperty("btoken")
) {
  radio = cStore.get("radio");
  if (cRadio !== null && typeof cRadio === "object") {
    if (cRadio.hasOwnProperty("btoken")) {
      cStore.append("log", "token from cStore " + new Date().toString());
    }
  }
} else {
  if (
    cRadio === null ||
    typeof cRadio !== "object" ||
    !cRadio.hasOwnProperty("btoken")
  ) {
    let cleanRadio = JSON.parse(JSON.stringify(radio));
    if (
      cleanRadio.hasOwnProperty("data") &&
      cleanRadio.data.hasOwnProperty("data") &&
      cleanRadio.data.data.hasOwnProperty("playlists")
    ) {
      delete cleanRadio.data.data.playlists;
    }
    cStore.set("radio", cleanRadio);
    cStore.set("lastupdate", new Date().toString());
    cStore.append("log", "token from store " + new Date().toString());
  } else {
    cStore.append("log", "both tokens " + new Date().toString());
  }
}

function checkToken(version, flag) {
  var result = { ok: false, removedBranch: false };
  // var ok = false;
  var paused = "wait";
  if (!flag) {
    player = $("#radioplayer").get(0);
    if (player) {
      paused = player.paused;
    }
  }
  if (
    radio !== null &&
    typeof radio === "object" &&
    radio.hasOwnProperty("btoken")
  ) {
    token = $.ajax({
      async: false,
      url: "http://api.millsonic.com/backend/v1/tokenBranch",
      type: "post",
      data: {
        btoken: radio.btoken,
        version: version,
        paused: paused,
        flag: flag,
        mac: JSON.stringify(MACaddresses),
      },
      error: function (errorObject, e2, e3) {
        console.log(errorObject, e2, e3);
        /*if(
          errorObject.status === 400 ||
          errorObject.statusText === "Bad Request"
        ){//si la API responde Bad Request el Token no es válido
          result.removedBranch = true;
        }//si es otro error se asume un error de conexión por lo que no se desconectará hasta que no reciba el error 400 "Bad Request"
        else {*/
        $("#loginError .text").text("Error de conexión");
        $("#loginError").removeClass("success");
        $("#loginError").fadeIn("slow");
        $("#playerError .text").text("Error de conexión");
        $("#playerError").removeClass("success");
        $("#playerError").fadeIn("slow");
        //}
      },
      success: function (response) {
        if (response.hasOwnProperty("branch")) {
          if(response.branch.hasOwnProperty("name")) {
            $(".branch").text(response.branch.name);
          }
          if (response.branch.hasOwnProperty("play")) {
            switch (parseInt(response.branch.play)) {
              default:
              case 0:
                $("#radiostatus").hide();
                break;
              case 1:
                playerPlay();
                $("#radiostatus").text("Server play");
                $("#radiostatus").show();
                break;
              case 2:
                playerPause();
                $("#radiostatus").text("Server pause");
                $("#radiostatus").show();
                break;
            }
          }
          if (response.branch.hasOwnProperty("volume")) {
            console.log("Volumen", response.branch.volume);
            playerChangeVolume(response.branch.volume);
          }
        }
      },
    });

    // Callback handler that will be called on success
    token.done(function (response, textStatus, jqXHR) {
      console.log(response);
      if (response.hasOwnProperty("status") && response.status === "fail") {
        if (response.msg == "Token Incorrecto") {
          result.removedBranch = true;
        }
      } else {
        if (
          response.cover ===
          "https://millsonic.com/sc/image/android-icon-192x192.png"
        ) {
          response.cover = "https://millsonic.com/sc/image/radiodefault.png";
        }
        $("#cover").attr("src", response.cover);
        $("#song").html(response.song);
        result.ok = true;

        if ($("#playerError .text").is(":visible")) {
          $("#playerError").addClass("success");
          $("#playerError .text").text("Conectando");
          setTimeout(function () {
            $("#playerError .text").text("");
            $("#playerError").fadeOut("slow");
          }, 2500);
        }

        if ($("#loginError .text").is(":visible")) {
          $("#loginError").addClass("success");
          $("#loginError .text").text("Conectando");
          setTimeout(function () {
            $("#loginError .text").text("");
            $("#loginError").fadeOut("slow");
          }, 2500);
        }
      }
    });
  } else {
    console.log("checkConnection");
    checkConnection = $.ajax({
      async: false,
      url: "http://api.millsonic.com/backend/v1/tokenBranch",
      type: "post",
      data: { btoken: 0, version: version, paused: paused, flag: flag },
      error: function (errorObject, e2, e3) {
        console.log(errorObject, e2, e3);
        if (
          !errorObject.status === 400 ||
          errorObject.statusText !== "Bad Request"
        ) {
          //si la API responde Bad Request el Token no es válido
          $("#loginError .text").text("Error de conexión");
          $("#loginError").removeClass("success");
          $("#loginError").fadeIn("slow");
        } else {
          if ($("#loginError .text").is(":visible")) {
            $("#loginError").addClass("success");
            $("#loginError .text").text("Conectando");
            setTimeout(function () {
              $("#loginError .text").text("");
              $("#loginError").fadeOut("slow");
            }, 2500);
          }
        }
      },
    });
    // Callback handler that will be called on success
    checkConnection.done(function (response, textStatus, jqXHR) {
      console.log("Weird", response);

      if ($("#loginError .text").is(":visible")) {
        $("#loginError").addClass("success");
        $("#loginError .text").text("Conectando");
        setTimeout(function () {
          $("#loginError .text").text("");
          $("#loginError").fadeOut("slow");
        }, 2500);
      }
    });
  }
  return result;
}

if (
  radio !== null &&
  typeof radio === "object" &&
  radio.hasOwnProperty("btoken")
) {
  var chckTk = checkToken(version, true);
  if (chckTk.ok) {
    $("#login").hide();
    $("#player").show();
    $("#playerrepo").append(
      "<audio controls autoplay style='display:none;' " +
        "id='radioplayer'><source src='" +
        radio.data.data.streamurldesktop +
        "' /></audio>"
    );
    $("#radioname").append(radio.data.data.name);
    console.log(radio.data.data);

    $(".play-btn").toggleClass("playing");
    playerPlaying = true;
  } else if (chckTk.removedBranch) {
    store.delete("radio");
    cStore.set("radio", null);
    cStore.set("lastupdate", new Date().toString());
    cStore.append(
      "log",
      "removed branch token not valid " + new Date().toString()
    );
    location.reload();
  }
} else {
  //REQUEST DEL CODIGO INICIAL
  var request;
  $("#code").submit(function (event) {
    event.preventDefault();
    if (request) {
      request.abort();
    }
    var $form = $(this);
    var $inputs = $form.find("input, select, button, textarea");
    var serializedData = $form.serialize();
    $inputs.prop("disabled", true);
    request = $.ajax({
      url: "http://api.millsonic.com/backend/v1/verifyBranch",
      type: "post",
      data: serializedData,
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
      $("#login").hide();
      console.log(response);
      let cleanRadio = JSON.parse(JSON.stringify(response));
      if (
        cleanRadio.hasOwnProperty("data") &&
        cleanRadio.data.hasOwnProperty("data") &&
        cleanRadio.data.data.hasOwnProperty("playlists")
      ) {
        delete cleanRadio.data.data.playlists;
      }
      cStore.set("radio", cleanRadio);
      cStore.set("lastupdate", new Date().toString());
      cStore.append("log", "verified branch " + new Date().toString());
      location.reload();
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
      // Log the error to the console
      alert(jqXHR.responseJSON.msg);
      console.error(
        "The following error occurred: " + textStatus,
        errorThrown,
        jqXHR.responseJSON
      );
    });
    request.always(function () {
      // Reenable the inputs
      $inputs.prop("disabled", false);
    });
  });
}

setInterval(function () {
  var domPlayer = document.getElementById("radioplayer");
  if (domPlayer !== null) {
    console.log(domPlayer.paused, domPlayer.duration, domPlayer.currentTime);
    if (lastTimeInput !== 0 && lastTimeInput === domPlayer.currentTime) {
      //el audio está parado
      console.log("player not advanced");
      if (playerPlaying) {
        //si el usuario no lo pausó, lo vuelve a cargar
        domPlayer.load();
        console.log("should play");
      } else {
        //si el usuario lo pausó, no hace nada
        console.log("user paused");
      }
    } else {
      if (isNaN(domPlayer.duration)) {
        //el audio no cargó
        console.log("not loaded");
        if (playerPlaying) {
          //si el usuario no lo pausó, lo vuelve a cargar
          domPlayer.load();
          console.log("should play");
        } else {
          //si el usuario lo pausó, no hace nada
          console.log("user paused");
        }
      } else {
        //está reproduciendo
        console.log("playing");
      }
    }
    if (domPlayer.duration > 0) {
      lastTimeInput = domPlayer.currentTime;
    }
  }

  var chckTk = checkToken(version, true);
  console.log(chckTk);
  if (chckTk.ok) {
    //OK
    if (domPlayer === null) {
      location.reload(); //conecta y tienen token
    }
  } else {
    if (chckTk.removedBranch) {
      disconect("removed branch"); //error: expiró el token
    }
  }
}, 3000);

function catchPaste(evt, elem, callback) {
  if (navigator.clipboard && navigator.clipboard.readText) {
    // modern approach with Clipboard API
    navigator.clipboard.readText().then(callback);
  } else if (evt.originalEvent && evt.originalEvent.clipboardData) {
    // OriginalEvent is a property from jQuery, normalizing the event object
    callback(evt.originalEvent.clipboardData.getData("text"));
  } else if (evt.clipboardData) {
    // used in some browsers for clipboardData
    callback(evt.clipboardData.getData("text/plain"));
  } else if (window.clipboardData) {
    // Older clipboardData version for Internet Explorer only
    callback(window.clipboardData.getData("Text"));
  } else {
    // Last resort fallback, using a timer
    setTimeout(function () {
      callback(elem.value);
    }, 100);
  }
}

function disconect(msg = "token disconnected") {
  store.delete("radio");
  cStore.append("log", msg + " " + new Date().toString());
  cStore.set("radio", null);
  cStore.set("lastupdate", new Date().toString());
  location.reload();
}

function getMac() {
  let allMACs = {};
  const netInters = require("os").networkInterfaces();
  console.log("os networkINterfaces", netInters);
  for (const netInterName in netInters) {
    if (netInters.hasOwnProperty(netInterName)) {
      const netInter = netInters[netInterName];
      const MACs = [];
      let mac = "";
      netInter.forEach((singleNetInter) => {
        if (singleNetInter.hasOwnProperty("mac")) {
          console.log("-->", netInterName, singleNetInter.mac);
          if (
            MACs.indexOf(singleNetInter.mac) == -1 &&
            singleNetInter.mac !== "00:00:00:00:00:00"
          ) {
            MACs.push(singleNetInter.mac);
          }
        }
      });
      if (MACs.length > 0) {
        allMACs[netInterName] = MACs.sort().join("|");
      }
    }
  }
  console.log("allMACs", allMACs);
  return allMACs;
}